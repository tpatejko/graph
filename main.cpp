#include "adjacency_list.hpp"

#include <iostream>
#include <iterator>

struct a {
    std::string name;

    bool operator==(const a& other) const {
        return name == other.name;
    }
};

/*
struct compare {
    bool operator()(const a& n1, const a& n2) const {
        return n1.name < n2.name;
    }
};
*/

int main() {
    adjacency_list<a> graph;
    
    a n1;
    a n2;
    a n3;
    a n4;
    a n5;
    a n6;
    
    n1.name = "a";
    n2.name = "b";
    n3.name = "c";
    n4.name = "d";
    n5.name = "e";
    n6.name = "f";
    
    graph.add_node(n1);
    graph.add_node(n2);
    graph.add_node(n3);
    graph.add_node(n4);
    graph.add_node(n5);
    graph.add_node(n6);

    graph.add_edge(n1, n2);
    graph.add_edge(n1, n3);
    graph.add_edge(n2, n3);
    graph.add_edge(n2, n4);
    graph.add_edge(n5, n3);

    for (auto it = graph.node_begin(); it != graph.node_end(); it++) {
        std::cout << "Vertex " << it->name << " ";
    }

    std::cout << "\n";
    
    for (auto it = graph.node_begin(); it != graph.node_end(); it++) {
        for (auto cit = graph.children_begin(*it); cit != graph.children_end(*it); cit++) {
            std::cout << it->name << " -> " << cit->name << std::endl;
        }
    }
        
    std::cout << "\n";

    auto order = topological_sort(graph);

    for (auto i : order) {
        std::cout << i.name << " ";
    }

    std::cout << "\n";
    
    for (auto ei = graph.edge_begin(); ei != graph.edge_end(); ei++) {
        if (*ei)
            std::cout << (*ei).value().first.name << " " << (*ei).value().second.name << "\n";
    }
    
    std::cout << "\n";
    
    return 0;
}
