#pragma once

#include <unordered_map>
#include <vector>
#include <algorithm>
#include <type_traits>
#include <list>
#include <iostream>

template<typename T>
struct adjacency_list {
    struct node {
        size_t id;
        T value;

        node(const T& v, size_t i) : value(v), id(i)
        {}
    };

    using value_type = T;
    using nodes_t = std::vector<node>;
    nodes_t nodes;
    std::vector<nodes_t> children;

    bool has_children(T n) const {
        auto n_node = find_node(n, nodes);
        
        return !children[n_node.value().id].empty();
    }
    
    bool is_child(T p, T c) const {
        auto p_idx = find_node(p);
        auto p_children = children[p_idx.id]; 
        
        if (!p_children.empty()) {
            return find_node(c, p_children);
        }
        return false;
    }
    
    void add_edge(T a, T b) {
        if (!is_node(a) || !is_node(b))
            return;

        auto a_node = find_node(a, nodes);
        auto b_node = find_node(b, nodes);
        
        children[a_node.value().id].push_back(b_node.value());
    }

    bool is_node(T v) const {
        return find_node(v, nodes).has_value();
    }

    std::optional<node> find_node(T n, const std::vector<node>& nodes) const {
        auto it = std::find_if(std::begin(nodes), std::end(nodes),
                               [&n](auto&& i) -> bool {
                                   return i.value == n;
                               });
               
        if (it != std::end(nodes)) {
            return *it;
        }

        return {};
    }
    
    void add_node(T n) {
        auto f = find_node(n, nodes);
        
        if (!f) {
            nodes.push_back(node(n, nodes.size()));
            children.push_back(nodes_t());
        }
    }
    
    class node_iterator {
        using iterator_category = std::forward_iterator_tag;
        using value_type = T;
        using difference_type = std::ptrdiff_t;
        using pointer = T*;
        using reference = T&;

        typename nodes_t::iterator it;

    public:
        node_iterator(typename nodes_t::iterator _it)
        : it(_it)
        {}

        node_iterator& operator=(const node_iterator& _it) = default;
        
        node_iterator& operator++() {
            ++it;
            return *this;
        }

        node_iterator operator++(int) {
            auto ret = *this;
            it++;
            return ret;
        }

        bool operator!=(node_iterator other) const {
            return it != other.it;
        }

        reference operator*() const {
            return it->value;
        }

        pointer operator->() const {
            return &it->value;
        }
    };
    
    node_iterator node_begin() {
        return node_iterator(nodes.begin());
    }

    node_iterator node_end() {
        return node_iterator(nodes.end());
    }

    node_iterator children_begin(T n) {
        auto node = find_node(n, nodes);
        return node_iterator(std::begin(children[node.value().id]));
    }

    node_iterator children_end(T n) {
        auto node = find_node(n, nodes);
        return node_iterator(std::end(children[node.value().id]));
    };

    class edge_iterator {
        using iterator_category = std::forward_iterator_tag;
        using value_type = std::pair<T, T>;
        using difference_type = std::ptrdiff_t;
        using pointer = std::pair<T, T>*;
        using reference = std::pair<T, T>&;

        int nidx;
        typename nodes_t::const_iterator cit;
        nodes_t& nodes;
        std::vector<nodes_t>& children;
  
      public:
        edge_iterator(int _nidx, typename nodes_t::const_iterator _cit, nodes_t& _nodes, std::vector<nodes_t>& _children)
        : nidx(_nidx)
        , cit(_cit)
        , children(_children)
        , nodes(_nodes)
        {}

        edge_iterator& operator++() {
            if (!children[nidx].empty())
                cit++;

             while (cit == std::end(children[nidx])) {
                nidx++;

                if (nidx == nodes.size()) {
                    cit = std::end(children[nidx-1]);
                    break;
                } else {
                    cit = std::begin(children[nidx]);
                }
            }

            return *this;
        }

        edge_iterator operator++(int) {
            auto ret = *this;

            ++(*this);

            return ret;
        }

        bool operator!=(edge_iterator other) {
            return !(nidx == other.nidx && cit == other.cit);
        }

        std::optional<std::pair<T, T>> operator*() {
            if (nodes.empty() || children[nidx].empty())
                return {};

            return std::make_pair(nodes[nidx].value, cit->value);
        }
    };

    edge_iterator edge_begin() {
        return edge_iterator(0, std::begin(children[0]), nodes, children);
    }

    edge_iterator edge_end() {
        return edge_iterator(nodes.size(), std::end(children[nodes.size()-1]), nodes, children);
    }
};

class ts {
    template<typename T>
    struct node {
        T value;
        int in_degree;

        node(const T& v, int in) : value(v), in_degree(in)
        {}
    };

  public:
    template<typename T>
    std::vector<T> operator()(adjacency_list<T>& graph) {
        std::vector<node<T>> in_degrees;
        
        for (auto it = graph.node_begin(); it != graph.node_end(); it++) {
            in_degrees.emplace_back(*it, 0);
        }

        for (auto nit = graph.node_begin(); nit != graph.node_end(); nit++) {
            for (auto cit = graph.children_begin(*nit); cit != graph.children_end(*nit); cit++) {
                auto it = std::find_if(std::begin(in_degrees), std::end(in_degrees),
                                       [c=*cit](const node<T>& n) -> bool {
                                           return c == n.value;
                                       });

                it->in_degree++;
            }
        }

        std::list<node<T>> next;
        std::copy_if(std::begin(in_degrees), std::end(in_degrees), std::back_inserter(next),
                     [](const node<T>& n)  -> bool {
                         return n.in_degree == 0;
                     });

        std::vector<T> order;
        
        while(!next.empty()) {
            auto u = next.front();
            order.push_back(u.value);
            next.pop_front();

            for (auto cit = graph.children_begin(u.value); cit != graph.children_end(u.value); cit++) {
                std::for_each(std::begin(in_degrees), std::end(in_degrees),
                              [c=*cit](node<T>& n) -> void {
                                  if (n.value == c) {
                                      n.in_degree--;
                                  }
                              });
                              
                auto it = std::find_if(std::begin(in_degrees), std::end(in_degrees),
                                       [c=*cit](const node<T>& n) -> bool {
                                           return n.value == c && n.in_degree == 0;
                                       });

                if (it != std::end(in_degrees))
                    next.push_front(*it);
            }
        }

        return order;
    }
};

template<typename T>
std::vector<T> topological_sort(adjacency_list<T>& graph) {
    ts t;
    return t(graph);
}
